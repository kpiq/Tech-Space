#!/bin/bash
exec > >(tee -a /var/log/generic-startup-script.log) 2>&1
echo "Script ${0} started at $(date)"

set -e
trap 'echo "Error on line $LINENO" >> /var/log/generic-startup-script.log' ERR

if [ "$0" = "/sbin/init" ]; then
  echo "${0} Running in shutdown mode"
  # Shutdown specific tasks
else
  echo "${0} Running in startup mode"
  # Startup specific tasks
fi

/usr/bin/systemctl stop spamassassin clamav
/usr/bin/systemctl disable spamassassin clamav

/usr/bin/apt purge -y spamassassin clamav

sudo ufw allow 22/tcp
sudo ufw allow 25/tcp
sudo ufw allow 143/tcp
sudo ufw allow 587/tcp
sudo ufw allow 993/tcp

/usr/sbin/ufw status verbose | /usr/bin/logger
