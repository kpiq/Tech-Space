#!/bin/bash
### USAGE: scriptname [d]
###    Where "d" is used to activate debugging.

### THE SCRIPT IS DESIGNED TO WORK WITH ONLY ONE WIREGUARD INTERFACE ACTIVE.

INTERFACE_NAME=$1
if [[ "$1" == "d" ]];
then
   INTERFACE_NAME=$2
   set -xv
fi

# Source system check script
source /usr/local/opnsense/scripts/system-check.sh
# Source init script
source /usr/local/opnsense/scripts/${INTERFACE_NAME}.ini

# Only interested in the first interface
# INTERFACE_NAME=$(wg show interfaces |head -1)

# Restore previous configuration based on the active time sync service
if [[ "$TIMESYNC_SERVICE" == "chrony" ]]; then
  mv /etc/chrony.conf.pre-wg /etc/chrony.conf
  systemctl restart chrony.service
elif [[ "$TIMESYNC_SERVICE" == "ntp" ]]; then
  mv /etc/ntp.conf.pre-wg /etc/ntp.conf
  systemctl restart ntp.service
elif [[ "$TIMESYNC_SERVICE" == "systemd-timesyncd" ]]; then
  mv /etc/systemd/timesyncd.conf.pre-wg /etc/systemd/timesyncd.conf
  systemctl restart systemd-timesyncd
fi
