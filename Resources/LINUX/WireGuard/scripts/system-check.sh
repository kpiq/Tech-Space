#!/bin/bash

# Function to check if systemd is being used
is_systemd() {
  # Further check if the only output is "systemd"
  if [[ $(ps -eo pid,comm | awk '$1 == "1" {print $2}') == "systemd" ]]; then
     return 0
  fi
  return 1
}

# Function to check if chrony service is active
is_chrony_active() {
  systemctl is-active chrony.service >/dev/null 2>&1 && return 0
  return 1
}

# Function to check if ntp service is active
is_ntp_active() {
  systemctl is-active ntp.service >/dev/null 2>&1 && return 0
  return 1
}

# Function to check if systemd-timesyncd service is active
is_timesyncd_active() {
  systemctl is-active systemd-timesyncd.service >/dev/null 2>&1 && return 0
  return 1
}

# Function to check if NetworkManager is active
is_networkmanager_active() {
  systemctl is-active NetworkManager.service >/dev/null 2>&1 && return 0
  return 1
}

# Main script execution

# Loop through all positional parameters (starting from index 1)
uCount=1
echo "$0 - $# positional parameters were passed to script."
for uParam in "$@"
do
  echo "Parameter #$uCount: $uParam"
  ((uCount++))  # Increment counter within loop
done

if is_systemd; then
  echo "Systemd detected."

  if is_chrony_active; then
    TIMESYNC_SERVICE="chrony"
  elif is_ntp_active; then
    TIMESYNC_SERVICE="ntp"
  elif is_timesyncd_active; then
    TIMESYNC_SERVICE="systemd-timesyncd"
  else
    echo "Warning: No active time synchronization service detected."
  fi

  if is_networkmanager_active; then
    USE_NMCLI=true
  else
    echo "NetworkManager not detected. Using alternative methods for configuration."
    USE_NMCLI=false
  fi
else
  echo "This script is designed for systemd-based systems."
fi

# Export variables for use in other scripts
export TIMESYNC_SERVICE USE_NMCLI
