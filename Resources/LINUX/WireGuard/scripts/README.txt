#AUTHOR: PEDRO SERRANO
#CITY: BAYAMON, PR 00959-2019
#DATE: 2024-05-04

The wgdnsup.sh and wgdnsdown.sh scripts can be invoked as follows from any /etc/wireguard/wginterface.conf file:

	PostUp = /usr/local/opnsense/scripts/wgdnsup.sh %i
	PostDown = /usr/local/opnsense/scripts/wgdnsdown.sh %i

Of course, you may choose to place those scripts in /usr/local/bin, or any other directory or your choice.

%i will pass the wireguard interface name (the name of the wginterface.conf file after truncating the .conf suffix) to the custom script.

To complete the configuration you must rename the wginterface.ini file to use the wireguard interface name (truncate .conf and keep the .ini suffix), and adjust the ipaddress, domain-name, and ntp server inside.
