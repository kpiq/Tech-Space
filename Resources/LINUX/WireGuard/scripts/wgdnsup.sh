#!/bin/bash
### USAGE: scriptname [d]
###    Where "d" is used to activate debugging.

### THE SCRIPT IS DESIGNED TO WORK WITH ONLY ONE WIREGUARD INTERFACE ACTIVE.

INTERFACE_NAME=$1
if [[ "$1" == "d" ]];
then
   INTERFACE_NAME=$2
   set -xv
fi

# Source system check script
source /usr/local/opnsense/scripts/system-check.sh
# Source init script
source /usr/local/opnsense/scripts/${INTERFACE_NAME}.ini

# Use variables exported by system-check.sh
# INTERFACE_NAME=$(wg show interfaces |head -1)

# Save current configuration based on the active time sync service
if [[ "$TIMESYNC_SERVICE" == "chrony" ]]; then
  if [[ ! -f /etc/chrony.conf.pre-wg ]]; then
    cp /etc/chrony.conf /etc/chrony.conf.pre-wg
    echo "Saved chrony configuration to /etc/chrony.conf.pre-wg"
  else
    echo "Backup of chrony configuration already exists. Skipping save."
  fi
elif [[ "$TIMESYNC_SERVICE" == "ntp" ]]; then
  if [[ ! -f /etc/ntp.conf.pre-wg ]]; then
    cp /etc/ntp.conf /etc/ntp.conf.pre-wg
    echo "Saved ntp configuration to /etc/ntp.conf.pre-wg"
  else
    echo "Backup of ntp configuration already exists. Skipping save."
  fi
elif [[ "$TIMESYNC_SERVICE" == "systemd-timesyncd" ]]; then
  if [[ ! -f /etc/systemd/timesyncd.conf.pre-wg ]]; then
    cp  /etc/systemd/timesyncd.conf /etc/systemd/timesyncd.conf.pre-wg
    echo "Saved ntp configuration to /etc/systemd/timesyncd.conf.pre-wg"
  else
    echo "Backup of systemd-timesyncd configuration already exists. Skipping save."
  fi
fi

# Configure DNS using systemd-resolved or NetworkManager (if applicable)
if [[ $USE_NMCLI == true ]]; then
  nmcli connection modify "$INTERFACE_NAME" ipv4.dns "$DNS_SERVER_IP"
  nmcli connection modify "$INTERFACE_NAME" ipv4.dns-search "$DOMAIN_SEARCH_LIST"
else
  # Implement alternative DNS configuration method (e.g., using resolv.conf)
  echo "Warning: NetworkManager not detected. Alternative DNS configuration not implemented."
fi

# Manage dns for this WG interface
if systemctl is-active --quiet systemd-resolved; then
   echo "systemd-resolved is active."
   resolvectl dns "$INTERFACE_NAME" "$DNS_SERVER_IP"
   resolvectl domain "$INTERFACE_NAME" "$DOMAIN_SEARCH_LIST"
   systemctl restart systemd-resolved
else
   echo "systemd-resolved is not active."
fi

# Add NTP server to the relevant configuration file (depending on service)
if [[ "$TIMESYNC_SERVICE" == "chrony" ]]; then
  echo "server $NTP_SERVER_IP iburst" >> /etc/chrony.conf
elif [[ "$TIMESYNC_SERVICE" == "ntp" ]]; then
  echo "pool $NTP_SERVER_IP" >> /etc/ntp.conf
elif [[ "$TIMESYNC_SERVICE" == "systemd-timesyncd" ]]; then
   echo "NTP=$NTP_SERVER_IP" >> /etc/systemd/timesyncd.conf
fi

# Restart the relevant time synchronization service
if [[ "$TIMESYNC_SERVICE" == "chrony" ]]; then
  systemctl restart chrony.service
elif [[ "$TIMESYNC_SERVICE" == "ntp" ]]; then
  systemctl restart ntp.service
elif [[ "$TIMESYNC_SERVICE" == "systemd-timesyncd" ]]; then
  systemctl restart systemd-timesyncd
fi
